from django.contrib import admin
from django.utils.html import format_html

# Register your models here.
from student.models import Student
admin.site.site_header = 'KONSTRUKTIV.UZ'
admin.site.index_title = 'konstruktiv.uz administration'
class StudentAdmin(admin.ModelAdmin):
    model = Student
    list_display = ['get_username','image_tag','get_first_name','get_last_name','address','mobile']
    search_fields = ['address']

    def get_first_name(self, obj):
        return obj.user.first_name

    def get_last_name(self, obj):
        return obj.user.last_name

    def get_username(self, obj):
        return obj.user.username

    def image_tag(self, obj):
        return format_html('<img src="{}" width="100px"/>'.format(obj.profile_pic.url))

    image_tag.short_description = 'Image'

    get_first_name.short_description = 'ISM'
    get_last_name.short_description = 'Familya'
    get_username.short_description = 'Username'

admin.site.register(Student,StudentAdmin)
