# Generated by Django 4.2.6 on 2023-11-03 19:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('student', '0002_remove_student_status'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='student',
            options={'verbose_name': "O'quvchi", 'verbose_name_plural': "O'quvchilar"},
        ),
    ]
