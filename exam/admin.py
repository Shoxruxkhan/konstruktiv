from django.contrib import admin
from ckeditor.widgets import CKEditorWidget
# Register your models here.
from django import forms
from exam.models import Course, Question, Result, Books
from django.utils.html import format_html

admin.site.register(Question)
admin.site.register(Result)

class BooksAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorWidget())
    class Meta:
        model = Books
        fields = '__all__'

class BooksAdmin(admin.ModelAdmin):
    form = BooksAdminForm
    list_display = ("books_name", "file_link")

class CourseAdmin(admin.ModelAdmin):
    list_display = ('course_name','question_number','total_marks',"image_tag",)
admin.site.register(Course,CourseAdmin)
admin.site.register(Books, BooksAdmin)