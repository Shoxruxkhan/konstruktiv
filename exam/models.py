from django.db import models
from django.db.models import Model
from ckeditor.fields import RichTextField

from onlinexam import settings
from student.models import Student
from django.utils.html import format_html
from django.utils.html import mark_safe

class Course(models.Model):
    course_image = models.ImageField(upload_to='course/', null=True, blank=True)
    course_name = models.CharField(max_length=50)
    question_number = models.PositiveIntegerField()
    total_marks = models.PositiveIntegerField()

    def image_tag(self):
        if self.course_image != '':
            return mark_safe('<img src="%s%s" width="150" height="150" />' % (f'{settings.MEDIA_URL}', self.course_image))
    def __str__(self):
        return self.course_name


class Books(models.Model):
    books_name = models.CharField(max_length=50)
    field_name = models.FileField(upload_to='Books', null=True, blank=True)
    content = RichTextField()
    geogebra_link = models.CharField(max_length=1024,null=True,blank=True)
    youtube_link = models.CharField(max_length=1024,null=True,blank=True)

    def file_link(self):
        if self.field_name:
            return format_html("<a href='%s'>download</a>" % (self.field_name.url,))
        else:
            return "No attachment"

    file_link.allow_tags = True

    def __str__(self):
        return self.books_name


class Question(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    marks = models.PositiveIntegerField()
    question = models.CharField(max_length=600)
    option1 = models.CharField(max_length=200)
    option2 = models.CharField(max_length=200)
    option3 = models.CharField(max_length=200)
    option4 = models.CharField(max_length=200)
    cat = (('variant 1', 'variant 1'), ('variant 2', 'variant 2'), ('variant 3', 'variant 3'), ('variant 4', 'variant 4'))
    answer = models.CharField(max_length=200, choices=cat)


class Result(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    exam = models.ForeignKey(Course, on_delete=models.CASCADE)
    marks = models.PositiveIntegerField()
    date = models.DateTimeField(auto_now=True)
