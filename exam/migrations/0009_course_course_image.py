# Generated by Django 3.0.5 on 2023-10-03 14:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0008_auto_20220430_0937'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='course_image',
            field=models.ImageField(blank=True, null=True, upload_to='course/Student/'),
        ),
    ]
